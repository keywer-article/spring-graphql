package com.keywer.masterclass.spring.graphql.graphql;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.keywer.masterclass.spring.graphql.model.Fish;
import com.keywer.masterclass.spring.graphql.model.Purchase;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.Random;


@Component
public class FishResolver implements GraphQLResolver<Fish> {

    public Purchase getPurchase(Fish fish) throws IOException {

        File purchasesDatasource = new File(getClass().getClassLoader().getResource("purchases.json").getFile());

        Purchase[] purchaseList = new ObjectMapper().readValue(purchasesDatasource, Purchase[].class);

        int randomIndex = new Random().nextInt(purchaseList.length);

        return purchaseList[randomIndex];
    }
}
