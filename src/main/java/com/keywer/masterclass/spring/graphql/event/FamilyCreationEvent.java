package com.keywer.masterclass.spring.graphql.event;

import com.keywer.masterclass.spring.graphql.model.Family;
import org.springframework.context.ApplicationEvent;

public class FamilyCreationEvent extends ApplicationEvent {
    private Family family;

    public FamilyCreationEvent(Object source, Family family) {
        super(source);
        this.family = family;
    }
    public Family getFamily() {
        return family;
    }
}