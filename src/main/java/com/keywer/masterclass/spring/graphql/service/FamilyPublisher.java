package com.keywer.masterclass.spring.graphql.service;

import com.keywer.masterclass.spring.graphql.event.FamilyCreationEvent;
import com.keywer.masterclass.spring.graphql.model.Family;
import com.keywer.masterclass.spring.graphql.model.Fish;
import com.keywer.masterclass.spring.graphql.model.WaterType;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.observables.ConnectableObservable;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Queue;
import java.util.concurrent.*;

@Component
public class FamilyPublisher {

    private Flowable<Family> publisher;

    private Queue<Family> events= new ArrayBlockingQueue<>(30);

    @Autowired
    public FamilyPublisher() {
        this.initSubscriber();
    }

    private void initSubscriber() {
        Observable<Family> familyObservable = Observable.create(observableEmitter -> {
                    ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
                    executorService.scheduleAtFixedRate(newFamilyTick(observableEmitter), 0, 2, TimeUnit.SECONDS);
                }
        );
        ConnectableObservable<Family> connectableObservable = familyObservable.share().publish();
        connectableObservable.connect();

        this.publisher = connectableObservable.toFlowable(BackpressureStrategy.BUFFER);
    }

    private Runnable newFamilyTick(ObservableEmitter<Family> observableEmitter) {
        return () -> {
            while(!events.isEmpty()) {
                observableEmitter.onNext(events.poll());
            }
        };
    }

    @EventListener
    public void handleSuccessful(FamilyCreationEvent event) {
        events.add(event.getFamily());
    }

    public Publisher<Family> getPublisher() {
        return publisher;
    }
}
