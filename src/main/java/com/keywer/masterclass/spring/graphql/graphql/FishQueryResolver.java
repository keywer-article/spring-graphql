package com.keywer.masterclass.spring.graphql.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.keywer.masterclass.spring.graphql.graphql.input.CursorInput;
import com.keywer.masterclass.spring.graphql.graphql.input.PaginationInput;
import com.keywer.masterclass.spring.graphql.model.Fish;
import com.keywer.masterclass.spring.graphql.service.FishDatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FishQueryResolver implements GraphQLQueryResolver {

    private FishDatabaseService fishDatabaseService;

    @Autowired
    public FishQueryResolver(FishDatabaseService fishDatabaseService) {
        this.fishDatabaseService = fishDatabaseService;
    }

    public Fish getMostExpensiveFish() {
        return fishDatabaseService.getMostExpensiveFish();
    }

    public Fish getFishByName(String name) {
        return fishDatabaseService.findFishByName(name);
    }

    public List<Fish> fishWithPagination(PaginationInput paginationInput) {
        Fish first = fishDatabaseService.findByOffset(paginationInput.getOffset());
        return fishDatabaseService.findFish(first.getId(), paginationInput.getFirst());
    }

    public List<Fish> fishWithCursor(CursorInput cursorInput) {
        return fishDatabaseService.findFish(cursorInput.getAfter(), cursorInput.getFirst());
    }

}
