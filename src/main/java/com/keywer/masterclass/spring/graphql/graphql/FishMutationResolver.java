package com.keywer.masterclass.spring.graphql.graphql;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.keywer.masterclass.spring.graphql.event.FamilyCreationEvent;
import com.keywer.masterclass.spring.graphql.model.Family;
import com.keywer.masterclass.spring.graphql.model.Fish;
import com.keywer.masterclass.spring.graphql.model.WaterType;
import com.keywer.masterclass.spring.graphql.repository.FamilyRepository;
import com.keywer.masterclass.spring.graphql.repository.FishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;
import java.util.Objects;

@Component
public class FishMutationResolver implements GraphQLMutationResolver {

    private ApplicationEventPublisher applicationEventPublisher;
    private final FamilyRepository familyRepository;
    private final FishRepository fishRepository;

    @Autowired
    public FishMutationResolver(ApplicationEventPublisher applicationEventPublisher,
                                FamilyRepository familyRepository,
                                FishRepository fishRepository) {
        this.applicationEventPublisher = applicationEventPublisher;
        this.familyRepository = familyRepository;
        this.fishRepository = fishRepository;
    }

    @Transactional
    public Family createFamily(String name, WaterType waterType) {
        Family family = Family.builder().name(name).waterType(waterType).build();
        FamilyCreationEvent customSpringEvent = new FamilyCreationEvent(this, family);
        applicationEventPublisher.publishEvent(customSpringEvent);
        return this.familyRepository.save(family);
    }

    @Transactional
    public Fish createFish(String name, int temperature, int price, String familyName) {
        Family family = this.familyRepository.findByName(familyName);

        if (Objects.isNull(family)) {
            throw new NoSuchElementException(String.format("Impossible to find Family %s", familyName));
        }
        Fish fish = Fish.builder().name(name).temperature(temperature).price(price).family(family).build();

        return this.fishRepository.save(fish);
    }
}
